#ifndef GET_INPUT_VOLTAGE_H
#define GET_INPUT_VOLTAGE_H

// Source:  https://forum.pjrc.com/threads/26117-Teensy-3-1-Voltage-sensing-and-low-battery-alert

void input_voltage_setup()
{
	analogReference(EXTERNAL);
	analogReadResolution(12);
	analogReadAveraging(32); // this one is optional.
}

// for Teensy 3.0, only valid between 2.0V and 3.5V. Returns in millivolts.
uint32_t get_input_voltage()
{
	uint32_t x = analogRead(39);
	return (178*x*x + 2688757565 - 1184375 * x) / 372346;
}

#endif
