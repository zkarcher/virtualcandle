//--------------------------------------------------------------------------
// Animated flame for Adafruit Pro Trinket.  Uses the following parts:
//   - Pro Trinket microcontroller (adafruit.com/product/2010 or 2000)
//     (#2010 = 3V/12MHz for longest battery life, but 5V/16MHz works OK)
//   - Charlieplex LED Matrix Driver (2946)
//   - Charlieplex LED Matrix (2947, 2948, 2972, 2973 or 2974)
//   - 350 mAh LiPoly battery (2750)
//   - LiPoly backpack (2124)
//   - SPDT Slide Switch (805)
//
// This is NOT good "learn from" code for the IS31FL3731; it is "squeeze
// every last byte from the Pro Trinket" code.  If you're starting out,
// download the Adafruit_IS31FL3731 and Adafruit_GFX libraries, which
// provide functions for drawing pixels, lines, etc.  This sketch also
// uses some ATmega-specific tricks and will not run as-is on other chips.
//--------------------------------------------------------------------------

#include <Wire.h>           // For I2C communication
#include <Adafruit_GFX.h>
#include "Adafruit_IS31FL3731_ZKA.h"
#include <Bounce.h>
#include "get_input_voltage.h"
#include "candle_data.h"

#define WIDTH      (9)
#define HEIGHT     (16)

const uint8_t ANIM_COUNT = 3;
const int8_t BATTERY_ANIM = -1;
const uint16_t SCRIM_TRANSITION_MS = 2.5f * 1000;
const uint16_t BATTERY_TRANSITION_MS = 2.0f * 1000;
const uint8_t SCRIM_HEIGHT_PX = 20;
const uint8_t SCRIM_OFFSCREEN_PX = 0;	// start with LEDs off
const uint8_t BATTERY_OFFSCREEN_PX = 3;

const int8_t DEFAULT_START_ANIM = 0;

const bool DEV_NO_SPEED_THROTTLE = false;
const bool DEV_PRINT_MILLIVOLTS_OVER_SERIAL = false;

Adafruit_IS31FL3731 ledmatrix = Adafruit_IS31FL3731();

uint8_t page = 0;    // Front/back buffer control
uint8_t img[WIDTH * HEIGHT]; // Buffer for rendering image

// CANDLE
const uint8_t *ptr  = CANDLE_ANIM; // Current pointer into animation data

// Plasma
// The lookup table to make the brightness changes be more visible
const uint8_t PLASMA_BRIGHT = 60;
float phase = 0.0f;

float heart_phase = 0.0f;

float battery_phase = 0.0f;

const uint8_t BUTTON_PIN = 12;
Bounce pushbutton = Bounce(BUTTON_PIN, 10);	// 10ms debounce

// Kludge to get around fake button input on sketch bootup
uint8_t ignore_button_frames = 10;

int8_t override_anim = 0;
int8_t anim = 0;
uint16_t scrim = 0;

uint8_t frame_count = 0;
const uint8_t LED_PIN = 13;

elapsedMillis since_draw;
uint16_t draw_interval_ms = 1000 / 60;

inline float lerp(float a, float b, float prog) {
	return a + (b-a)*prog;
}

inline float dual_osc(float value, float harmonic) {
	return (sinf(value) + sinf(value * harmonic)) * 0.5f;
}

// Begin I2C transmission and write register address (data then follows)
void writeRegister(uint8_t n) {
  Wire.beginTransmission(ISSI_ADDR_DEFAULT);
  Wire.write(n);
  // Transmission is left open for additional writes
}

// Select one of eight IS31FL3731 pages, or Function Registers
void pageSelect(uint8_t n) {
  writeRegister(ISSI_COMMANDREGISTER); // Command Register
  Wire.write(n);       // Page number (or 0xB = Function Registers)
  Wire.endTransmission();
}

inline uint8_t img_idx(uint8_t x, uint8_t y) {
	return ((WIDTH-1)-x) * HEIGHT + y;
}

inline void clear_img() {
	memset(img, 0, sizeof(img) / sizeof(img[0]));
}

void set_anim(int8_t value) {
	if (value < 0) {
		override_anim = value;

	} else {
		override_anim = 0;
		anim = value;
	}

	// Animations have different frame rates
	switch (value) {
		case 0:    // candle
			{draw_interval_ms = 1000 / 24; break;}

		case 1:    // plasma
			{draw_interval_ms = 1; break;}

			//{draw_interval_ms = 0; break;}	// as fast as possible!

		case -1:	// battery
		case 2:    // heart
		default:
			{draw_interval_ms = 1000 / 60; break;}	// 60 FPS
	}

	ledmatrix.clear();
	clear_img();
	scrim = 0;
}

void setup() {
	if (DEV_PRINT_MILLIVOLTS_OVER_SERIAL) {
		Serial.begin(9600);
	}

	if (!ledmatrix.begin()) {
		//Serial.println("IS31 not found");
		digitalWrite(LED_PIN, HIGH);
		while (1);
	}
	//Serial.println("IS31 found!");

	pinMode(BUTTON_PIN, INPUT);
	pinMode(LED_PIN, OUTPUT);

	input_voltage_setup();

	clear_img();
	ledmatrix.clear();

	digitalWrite(LED_PIN, LOW);

	set_anim(DEFAULT_START_ANIM);
}

void loop() {

	if (ignore_button_frames > 0) {
		ignore_button_frames--;
		pushbutton.update();

	} else {
		if (pushbutton.update()) {
			bool is_falling = pushbutton.fallingEdge();

			if (is_falling) {	// On button down
				set_anim(BATTERY_ANIM);

			} else {	// On button up
				set_anim((anim + 1) % ANIM_COUNT);
			}

			// On press or release: Immediately force a redraw
			since_draw = draw_interval_ms;
		}
	}

	uint8_t millis_this_frame;

	if (!DEV_NO_SPEED_THROTTLE) {
		if (since_draw < draw_interval_ms) {
			return;
		}

		if (scrim < SCRIM_TRANSITION_MS) {
			scrim += since_draw;
		}

		millis_this_frame = since_draw;
		since_draw = 0;
	}

	// Display frame rendered on prior pass.  This is done at function start
  // (rather than after rendering) to ensire more uniform animation timing.
  pageSelect(ISSI_BANK_FUNCTIONREG);    // Function registers
  writeRegister(ISSI_REG_PICTUREFRAME); // Picture Display reg
  Wire.write(page);    // Page #
  Wire.endTransmission();

  page ^= 1; // Flip front/back buffer index

	int8_t do_anim = anim;
	if (override_anim != 0) do_anim = override_anim;

	// When the button is down: Battery animation
	if (do_anim == BATTERY_ANIM) {
		uint8_t idx;

		const uint8_t OUTLINE_COLOR = 0x40;
		const uint8_t FILL_COLOR = 0x10;

		battery_phase += 0.0022f * millis_this_frame;

		// Expected range is about 2000 - 3500
		uint32_t millivolts = get_input_voltage();
		uint16_t mv_clamp = constrain(millivolts, 2000, 3500);
		float full_amt = ((float)mv_clamp - 2000.0f) / (3500.0f - 2000.0f);
		float full_height = (HEIGHT-3) * full_amt;

		uint8_t surface = (uint8_t)(full_height);

		for (uint8_t h=0; h<=full_height; h++) {

			for (uint8_t x=2; x<WIDTH-2; x++) {
				idx = img_idx(x, HEIGHT-2-h);
				img[idx] = FILL_COLOR;

				if (h == surface) {
					float rando = (sinf(battery_phase - x) * 0.5f + 0.5f);
					img[idx] = lerp( 0, FILL_COLOR, rando );
				}
			}
		}

		// Battery outline
		for (uint8_t y=1; y<HEIGHT; y++) {
			idx = img_idx(1,y);
			img[idx] = OUTLINE_COLOR;
			idx = img_idx(WIDTH-2,y);
			img[idx] = OUTLINE_COLOR;
		}

		for (uint8_t x=1; x<WIDTH-1; x++) {
			idx = img_idx(x,HEIGHT-1);
			img[idx] = OUTLINE_COLOR;

			// Nub on top
			if ((3 <= x) && (x <= 5)) {
				idx = img_idx(x,0);
				img[idx] = OUTLINE_COLOR;
			}

			// Top edge
			if (x != 4) {
				idx = img_idx(x,1);
				img[idx] = OUTLINE_COLOR;
			}
		}

		if (DEV_PRINT_MILLIVOLTS_OVER_SERIAL) {
			Serial.println(millivolts);
		}

	} else if (do_anim == 0) {
	//if (new_button_state == LOW) {
		// CANDLE
		uint8_t  a, x1, y1, x2, y2, x, y;

		// Then render NEXT frame.  Start by getting bounding rect for new frame:
		a = pgm_read_byte(ptr++);     // New frame X1/Y1
		if(a >= 0x90) {               // EOD marker? (valid X1 never exceeds 8)
			ptr = CANDLE_ANIM;          // Reset animation data pointer to start
			a   = pgm_read_byte(ptr++); // and take first value
		}
		x1 = a >> 4;                  // X1 = high 4 bits
		y1 = a & 0x0F;                // Y1 = low 4 bits
		a  = pgm_read_byte(ptr++);    // New frame X2/Y2
		x2 = a >> 4;                  // X2 = high 4 bits
		y2 = a & 0x0F;                // Y2 = low 4 bits

		for(x=x1; x<=x2; x++) { // Column-major
			for(y=y1; y<=y2; y++) {
				//uint8_t idx = img_idx(x,y);
				uint8_t idx = img_idx((WIDTH-1)-x,y);	// flip horizontally, because I like it better
				img[idx] = pgm_read_byte(ptr++);
			}
		}

	} else if (do_anim == 1) {
		// Plasma
		phase += 0.00085f * millis_this_frame;

		float wave_width = dual_osc(phase * 0.24f, 0.77f);
		wave_width = 0.845f + wave_width * 0.15f;

		for (uint8_t x = 0; x < WIDTH; x++) {
			float offset_y = dual_osc(x * wave_width + phase * 0.37f, 0.49f);

			for (uint8_t y = 0; y < HEIGHT; y++) {
				uint8_t idx = img_idx(x,y);

				float brt = (1.0f - abs(dual_osc(y * 0.47f + offset_y + phase, 0.66f)));
				brt *= brt;	// cheap gamma correction

				img[idx] = brt * PLASMA_BRIGHT;
			}
		}

	} else if (do_anim == 2) {	// heart!
		const uint16_t heart[] = {
			0b011000110,	// 0
			0b111101111,
			0b111111111,
			0b111111111,
			0b111111111,
			0b111111111,	// 5
			0b011111110,
			0b001111100,
			0b000111000,
			0b000010000,	// 9
		};

		heart_phase += 0.0035f * millis_this_frame;

		for (uint8_t y=0; y<10; y++) {
			uint16_t byte = heart[y];
			for (uint8_t x=0; x<WIDTH; x++) {
				uint16_t is_on = byte & (1<<x);

				if (is_on) {
					uint8_t idx = img_idx(x,y+3);

					//float spd = (x + 0.3f) * (y + 0.2f);
					float spd = powf(x + 0.3f, y + 0.2f);
					spd -= floor(spd);

					float brt = dual_osc(spd * heart_phase, 0.87f) * 0.5f + 0.5f;
					brt *= brt;
					brt *= brt;

					img[idx] = lerp( 7, 80, brt );
				}
			}
		}
	}

	// Apply the scrim (if any)
	uint16_t scrim_time_ms = (do_anim < 0) ? BATTERY_TRANSITION_MS : SCRIM_TRANSITION_MS;

	if (scrim < scrim_time_ms) {
		float prog = float(scrim) / scrim_time_ms;
		uint8_t offscreen_px = (do_anim < 0) ? BATTERY_OFFSCREEN_PX : SCRIM_OFFSCREEN_PX;
		float scrim_bottom = (1.0f - prog) * (HEIGHT + SCRIM_HEIGHT_PX + offscreen_px);
		float scrim_top = scrim_bottom - SCRIM_HEIGHT_PX;

		for (uint8_t y=0; y<HEIGHT; y++) {
			uint8_t mult = 0;
			if (y >= scrim_bottom) {
				mult = 0xff;
			} else {
				mult = 0xff * ((y - scrim_top) / SCRIM_HEIGHT_PX);

				// Fake gamma correction
				mult = (mult * mult) >> 8;
			}

			for (uint8_t x=0; x<WIDTH; x++) {
				uint8_t idx = img_idx(x,y);
				img[idx] = (img[idx] * mult) >> 8;
			}
		}
	}

	// Write img[] to the matrix
	pageSelect(page);    // Select background buffer
	writeRegister(0x24); // First byte of PWM data
	uint8_t i = 0, byteCounter = 1;
	for(uint8_t x=0; x<WIDTH; x++) {
		for(uint8_t y=0; y<HEIGHT; y++) {
			Wire.write(img[i++]);      // Write each byte to matrix
			if(++byteCounter >= 32) {  // Every 32 bytes...
				byteCounter = 1;         // ZKA: Added this line, to ensure "Every 32 bytes..."
				Wire.endTransmission();  // end transmission and
				writeRegister(0x24 + i); // start a new one (Wire lib limits)
			}
		}
	}
	Wire.endTransmission();

	/*
	frame_count = (frame_count + 1) % 60;
	if (frame_count == 0) {
		digitalWrite(LED_PIN, HIGH);
	} else {
		digitalWrite(LED_PIN, LOW);
	}
	*/
}
